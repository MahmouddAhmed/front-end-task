const table = document.getElementById("pixelCanvas");
// Select color input
const color = document.getElementById("colorPicker");
var colorvalue = color.value;
color.addEventListener("change", function() {
  colorvalue = color.value;
});
// Select size input
const height = document.getElementById("inputHeight");
var heightvalue = height.value;
height.addEventListener("change", function() {
  heightvalue = height.value;
});

const width = document.getElementById("inputWidth");
var widthvalue = width.value;
width.addEventListener("change", function() {
  widthvalue = width.value;
});
// When size is submitted by the user, call makeGrid()
document.getElementById("sizePicker").addEventListener("submit", makeGrid);

function makeGrid(e) {
  e.preventDefault();
  while (table.hasChildNodes()) {
    table.removeChild(table.firstChild);
  }
  for (let i = 0; i < heightvalue; i++) {
    var row = table.insertRow(i);
    for (let j = 0; j < widthvalue; j++) {
      var cell = row.insertCell(j);
      cell.addEventListener("click", function(e) {
        e.target.style.backgroundColor = colorvalue;
      });
    }
  }
}